﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Search.Core.Test
{
    [TestFixture]
    public class SearchEnginePerformanceTests
    {
        //An attempt to write test to detect memory leak base on memory usage
        [Test]
        [Category("SearchEngine_Performance")]
        public void Memory_Leak_Search_Test()
        {

            //Arrange
            List<string> patterns = new List<string> { "you", "why", "tended mutual" };
            MemoryProfiler profiler = new MemoryProfiler();

            using (MemoryStream ms = new MemoryStream())
            {
                ms.Write(@"Remain valley who mrs uneasy remove wooded him you. Her questions favourite him concealed. 
We to wife face took he.The taste begin early old WHY since dried can first.Prepared as or
humoured formerly.Evil mrs true get post.Express village evening prudent my as ye
hundred forming.Thoughts she why not directly reserved packages yOu.Winter an silent favour of
am   tended      muTual.
");

                using (StreamReader sr = new StreamReader(ms))
                {
                    for (int j = 0; j < 20; j++) //200 000 in total
                    {
                        for (int i = 0; i < 10000; i++)
                        {
                            SearchEngine se = new SearchEngine();

                            ms.Position = 0;
                            //Act
                            se.Search(sr, patterns);
                        }

                        if (profiler.VerifyMemory(15))
                            return;

                    }

                    Assert.Fail();

                }

            }
        }
        [Test]
        [Category("SearchEngine_Performance")]
        public void Performance_Search_Test()
        {

            //Arrange
            List<string> patterns = new List<string> { "you", "why", "tended mutual" };
            var watch = System.Diagnostics.Stopwatch.StartNew();

            using (MemoryStream ms = new MemoryStream())
            {
                ms.Write(@"Remain valley who mrs uneasy remove wooded him you. Her questions favourite him concealed. 
We to wife face took he.The taste begin early old WHY since dried can first.Prepared as or
humoured formerly.Evil mrs true get post.Express village evening prudent my as ye
hundred forming.Thoughts she why not directly reserved packages yOu.Winter an silent favour of
am   tended      muTual.
");

                using (StreamReader sr = new StreamReader(ms))
                {
                    for (int j = 0; j < 100000; j++)
                    {
                        SearchEngine se = new SearchEngine();

                        ms.Position = 0;
                        //Act
                        se.Search(sr, patterns);
                    }
                }

            }
            watch.Stop();
            Assert.That(watch.ElapsedMilliseconds, Is.LessThan(3000));
        }
    }
}
