﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Search.Core.Test
{
    public class MemoryProfiler
    {
        private long _lastCheckPointByteCount;
        private List<long> _successCheckPointList = new List<long>();

        public bool VerifyMemory(int sucessCheckPointCountCriteria = 7, long delta = 1000)
        {
            GC.Collect(GC.MaxGeneration, GCCollectionMode.Forced);
            GC.WaitForPendingFinalizers();

            long finalByteCount = GC.GetTotalMemory(true);
            if (Math.Abs(finalByteCount - _lastCheckPointByteCount) < delta)
            {
                _successCheckPointList.Add(finalByteCount);
            }
            else
            {
                _successCheckPointList.Clear();
            }

            _lastCheckPointByteCount = finalByteCount;

            return _successCheckPointList.Count >= sucessCheckPointCountCriteria && VerifyDeltaForCheckPoints(delta);
        }

        private bool VerifyDeltaForCheckPoints(long delta)
        {
            foreach (var checkpoint in _successCheckPointList)
            {
                foreach (var checkPointToCompare in _successCheckPointList)
                {
                    if (Math.Abs(checkpoint - checkPointToCompare) > delta)
                    {
                        return false;
                    }
                }
            }
            return true;
        }
    }
}
