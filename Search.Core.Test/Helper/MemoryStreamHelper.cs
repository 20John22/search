﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Search.Core.Test
{
    public static class MemoryStreamHelper
    {
        public static void WriteLine(this MemoryStream ms, string line)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(line + Environment.NewLine);
            ms.Write(bytes, 0, bytes.Length);
        }
        public static void Write(this MemoryStream ms, string text)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(text);
            ms.Write(bytes, 0, bytes.Length);
        }
    }
}
