﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace Search.Core.Test
{
    [TestFixture]
    public class SearchEngineTest
    {
        [Test]
        [Ignore("test for file")]
        [Category("SearchEngine")]
        public void Return_Result_For_Sample_File()
        {
            //Arrange
            SearchEngine se = new SearchEngine();
            List<string> patterns = new List<string> { "New Zealand", "international", "population", "oecd" };

            var assemblyPath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembl‌​y().Location);

            using (StreamReader sr = new StreamReader(assemblyPath + @"\Files\TextToValidate.txt"))
            {
                //Act
                var searchResult = se.Search(sr, patterns);

                //Assert
                //Assert.AreEqual(searchResult["tended mutual"].Count, 1);
                //Assert.AreEqual(searchResult["tended mutual"][0].Line, 5);
                //Assert.AreEqual(searchResult["tended mutual"][0].Column, 4);
            }

        }

        [Test]
        [Category("SearchEngine")]
        public void Return_Results_For_Sample_Text()
        {
            //Arrange
            SearchEngine se = new SearchEngine();
            List<string> patterns = new List<string> { "you", "why", "tended mutual" };
            using (MemoryStream ms = new MemoryStream())
            {
                ms.Write(@"Remain valley who mrs uneasy remove wooded him you. Her questions favourite him concealed. 
We to wife face took he.The taste begin early old why since dried can first.Prepared as or
humoured formerly.Evil mrs true get post.Express village evening prudent my as ye
hundred forming.Thoughts she why not directly reserved packages you.Winter an silent favour of
am tended mutual.
");
                ms.Position = 0;

                using (StreamReader sr = new StreamReader(ms))
                {
                    //Act
                    var searchResult = se.Search(sr, patterns);

                    //Assert
                    Assert.AreEqual(searchResult["you"].Count, 2);
                    Assert.AreEqual(searchResult["you"][0].Line, 1);
                    Assert.AreEqual(searchResult["you"][0].Column, 48);


                    Assert.AreEqual(searchResult["why"].Count, 2);
                    Assert.AreEqual(searchResult["why"][0].Line, 2);
                    Assert.AreEqual(searchResult["why"][0].Column, 51);
                    Assert.AreEqual(searchResult["why"][1].Line, 4);
                    Assert.AreEqual(searchResult["why"][1].Column, 30);

                    Assert.AreEqual(searchResult["tended mutual"].Count, 1);
                    Assert.AreEqual(searchResult["tended mutual"][0].Line, 5);
                    Assert.AreEqual(searchResult["tended mutual"][0].Column, 4);
                }
            }
        }
        [Test]
        [Category("SearchEngine")]
        public void Throws_Exception_For_Null_Stream_Or_Pattern_List()
        {
            //Arrange
            SearchEngine se = new SearchEngine();
            using (MemoryStream ms = new MemoryStream())
            {
                ms.Position = 0;

                using (StreamReader sr = new StreamReader(ms))
                {
                    //Act
                    Assert.Throws(typeof(ArgumentNullException),
                        delegate { se.Search(sr, null); });
                    Assert.Throws(typeof(ArgumentNullException),
                        delegate { se.Search(null, new List<string> { "test" }); });
                }
            }
        }
        [Test]
        [Category("SearchEngine")]
        public void Returns_Empty_Result_For_Empty_Input_Stream()
        {
            //Arrange
            SearchEngine se = new SearchEngine();
            List<string> patterns = new List<string> { "you", "why", "tended mutual" };
            using (MemoryStream ms = new MemoryStream())
            {
                ms.Position = 0;

                using (StreamReader sr = new StreamReader(ms))
                {
                    //Act
                    var searchResult = se.Search(sr, patterns);

                    //Assert
                    Assert.AreEqual(searchResult.Count, 0);
                }
            }
        }
        [Test]
        [Category("SearchEngine")]
        public void Returns_Empty_Result_For_Empty_Pattern_List()
        {
            //Arrange
            SearchEngine se = new SearchEngine();
            List<string> patterns = new List<string> { };
            using (MemoryStream ms = new MemoryStream())
            {
                ms.Write(@"Remain valley who mrs uneasy remove wooded him you. Her questions favourite him concealed. 
We to wife face took he.The taste begin early old WHY since dried can first.Prepared as or
humoured formerly.Evil mrs true get post.Express village evening prudent my as ye
hundred forming.Thoughts she why not directly reserved packages yOu.Winter an silent favour of
am tended muTual.
");
                ms.Position = 0;

                using (StreamReader sr = new StreamReader(ms))
                {
                    //Act
                    var searchResult = se.Search(sr, patterns);

                    //Assert
                    Assert.AreEqual(searchResult.Count, 0);
                }
            }
        }

        [Test]
        [Category("SearchEngine")]
        public void Throws_Exception_For_Pattern_List_With_Empty_Or_Null_Values()
        {
            //Arrange
            SearchEngine se = new SearchEngine();
            List<string> patterns = new List<string> { "", null};
            using (MemoryStream ms = new MemoryStream())
            {
                ms.Write(@"test");
                ms.Position = 0;

                using (StreamReader sr = new StreamReader(ms))
                {
                    //Act and Assert
                    Assert.Throws(typeof(ArgumentException), delegate { se.Search(sr, patterns); });
                }
            }
        }
        [Test]
        [Category("SearchEngine")]
        public void Can_Match_Upper_And_Lower_Case_Pattern()
        {
            //Arrange
            SearchEngine se = new SearchEngine();
            List<string> patterns = new List<string> { "you", "why", "tended mutual" };
            using (MemoryStream ms = new MemoryStream())
            {
                ms.Write(@"Remain valley who mrs uneasy remove wooded him you. Her questions favourite him concealed. 
We to wife face took he.The taste begin early old WHY since dried can first.Prepared as or
humoured formerly.Evil mrs true get post.Express village evening prudent my as ye
hundred forming.Thoughts she why not directly reserved packages yOu.Winter an silent favour of
am tended muTual.
");
                ms.Position = 0;

                using (StreamReader sr = new StreamReader(ms))
                {
                    //Act
                    var searchResult = se.Search(sr, patterns);

                    //Assert
                    Assert.AreEqual(searchResult["you"].Count, 2);
                    Assert.AreEqual(searchResult["you"][0].Line, 1);
                    Assert.AreEqual(searchResult["you"][0].Column, 48);


                    Assert.AreEqual(searchResult["why"].Count, 2);
                    Assert.AreEqual(searchResult["why"][0].Line, 2);
                    Assert.AreEqual(searchResult["why"][0].Column, 51);
                    Assert.AreEqual(searchResult["why"][1].Line, 4);
                    Assert.AreEqual(searchResult["why"][1].Column, 30);

                    Assert.AreEqual(searchResult["tended mutual"].Count, 1);
                    Assert.AreEqual(searchResult["tended mutual"][0].Line, 5);
                    Assert.AreEqual(searchResult["tended mutual"][0].Column, 4);
                }
            }
        }
        [Test]
        [Category("SearchEngine")]
        public void Can_Math_Sentence_With_Multiple_Whitespaces()
        {
            //Arrange
            SearchEngine se = new SearchEngine();
            List<string> patterns = new List<string> { "you", "why", "tended mutual" };
            using (MemoryStream ms = new MemoryStream())
            {
                ms.Write(@"Remain valley who mrs uneasy remove wooded him you. Her questions favourite him concealed. 
We to wife face took he.The taste begin early old WHY since dried can first.Prepared as or
humoured formerly.Evil mrs true get post.Express village evening prudent my as ye
hundred forming.Thoughts she why not directly reserved packages yOu.Winter an silent favour of
am   tended      muTual.
");
                ms.Position = 0;

                using (StreamReader sr = new StreamReader(ms))
                {
                    //Act
                    var searchResult = se.Search(sr, patterns);

                    //Assert
                    Assert.AreEqual(searchResult["tended mutual"].Count, 1);
                    Assert.AreEqual(searchResult["tended mutual"][0].Line, 5);
                    Assert.AreEqual(searchResult["tended mutual"][0].Column, 6);
                }
            }
        }

        [Test]
        [Category("SearchEngine")]
        [Ignore("no time to implement this feature https://stackoverflow.com/questions/1962220/apply-a-regex-on-stream")]
        public void Can_Match_Sentence_Broken_To_New_Line()
        {
            //Arrange
            SearchEngine se = new SearchEngine();
            List<string> patterns = new List<string> { "you", "why", "tended mutual" };
            using (MemoryStream ms = new MemoryStream())
            {
                ms.Write(@"Remain valley who mrs uneasy remove wooded him you. Her questions favourite him concealed. 
We to wife face took he.The taste begin early old WHY since dried can first.Prepared as or
humoured formerly.Evil mrs true get post.Express village evening prudent my as ye
hundred forming.Thoughts she why not directly reserved packages yOu.Winter an silent favour of
am   tended      
muTual.
");
                ms.Position = 0;

                using (StreamReader sr = new StreamReader(ms))
                {
                    //Act
                    var searchResult = se.Search(sr, patterns);

                    //Assert
                    Assert.AreEqual(searchResult["tended mutual"].Count, 1);
                    Assert.AreEqual(searchResult["tended mutual"][0].Line, 5);
                    Assert.AreEqual(searchResult["tended mutual"][0].Column, 4);
                }
            }
        }
    }

}
