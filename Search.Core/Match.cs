﻿namespace Search.Core
{
    public struct Match
    {
        public int Line { get; set; }
        public int Column { get; set; }
        public string Text { get; set; }
    }
}