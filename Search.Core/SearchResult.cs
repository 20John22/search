﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Search.Core
{
    public class SearchResult : Dictionary<string, List<Match>>
    {
        private List<string> _patterns;

        public SearchResult(List<string> patterns)
        {
            _patterns = patterns;
        }

        internal void AddMatch(string value, int lineNumber, int columnNumber)
        {
            string key = Regex.Replace(value.ToLower(), @"\s+", " ");
            if (this.ContainsKey(key))
            {
                this[key].Add(new Match { Line = lineNumber, Column = columnNumber, Text = value });
            }
            else
            {
                this.Add(key, new List<Match> { new Match { Line = lineNumber, Column = columnNumber, Text = value } });
            }
        }
    }
}