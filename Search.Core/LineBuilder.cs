﻿using System.IO;
using System.Linq;
using System.Text;

namespace Search.Core
{
    public class LineBuilder
    {
        private string _currentStreamLine;
        private readonly StreamReader _streamReader;

        public LineBuilder(StreamReader streamReader)
        {
            _currentStreamLine = "";
            _streamReader = streamReader;
        }

        public string GetNextLine()
        {
            StringBuilder sb = new StringBuilder();
            //check remining text
            if(_currentStreamLine.Contains("."))
                //retrn line
            sb.Append(_currentStreamLine);
           
            while ((_currentStreamLine = _streamReader.ReadLine()) != null && !_currentStreamLine.Contains("."))
            {
                sb.Append(_currentStreamLine);
            }

            var lines = _currentStreamLine.Split('.');
            sb.Append(lines[0]);

            _currentStreamLine = ReturnRemainingText(lines);
            return sb.ToString();
        }

        private string ReturnRemainingText(string[] lines)
        {
            if (lines.Count() <= 1)
                return "";
            return string.Join(".", lines.Skip(1));
        }
    }
}
