﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Search.Core
{
    public interface ISearchEngine
    {
        List<SearchResult> Search(string input, List<string> wordsAndSentences);

    }
}
