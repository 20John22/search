﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Search.Core
{
    public class SearchEngine
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="stream">stream with input text</param>
        /// <param name="patterns">list of words or sentences</param>
        /// <returns></returns>
        public SearchResult Search(StreamReader stream, List<string> patterns)
        {
            if (stream == null || patterns == null)
                throw new ArgumentNullException();
            if (patterns.Any(p => string.IsNullOrEmpty(p)))
                throw new ArgumentException("patterns");

            SearchResult searchResult = new SearchResult(patterns);
            if (patterns.Count == 0)
                return searchResult;

            string pattern = PrepareRegexPattern(patterns);
            string line;
            int lineNumber = 0;
            //TODO refactor to read by sentence instead of by line
            while ((line = stream.ReadLine()) != null)
            {
                ++lineNumber;
                MatchCollection results = Regex.Matches(line, pattern, RegexOptions.IgnoreCase);
                foreach (System.Text.RegularExpressions.Match result in results)
                {
                    searchResult.AddMatch(result.Value, lineNumber, result.Index + 1);
                }
            }

            return searchResult;
        }

        private string PrepareRegexPattern(List<string> patterns)
        {
            return string.Format(@"\b({0})\b", string.Join("|", patterns.Select(p => p.Replace(" ", @"\s*"))));
        }
    }
}
