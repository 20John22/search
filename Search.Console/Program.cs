﻿using Search.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Search.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Count() != 2)
                System.Console.WriteLine("Invlaid arguments");

            string pathToText = args[0];
            string pathToPatternFile = args[1];
            if(!Directory.Exists(pathToText))
                System.Console.WriteLine("pathToText doesn't exists");
            if (!Directory.Exists(pathToPatternFile))
                System.Console.WriteLine("pathToPatternFile doesn't exists");

            List<string> patterns = new List<string>();
            SearchEngine se = new SearchEngine();

            using (StreamReader sr = new StreamReader(pathToPatternFile))
            {
                string pattern;
                while((pattern = sr.ReadLine()) != null)
                {
                    patterns.Add(pattern);
                }
            }

            using (StreamReader sr = new StreamReader(pathToText))
            {
                //Act
                var searchResult = se.Search(sr, patterns);
                System.Console.WriteLine("Result:");
                foreach (var result in searchResult)
                {
                    foreach (var match in result.Value)
                    {
                        System.Console.WriteLine("Line: {0}, Col: {1}, “{2}”", match.Line, match.Column, match.Text);
                        //Line: 1, Col: 48, “you”
                        //Line: 2, Col: 52, “why”
                        //Line: 4, Col: 31, “why”
                        //Line: 5, Col: 4, “tended mutual”
   
                    }
                }
                System.Console.WriteLine("Summry:");
                foreach (var result in searchResult)
                {
                    System.Console.WriteLine("“{0}”: {1}", result.Key, result.Value.Count);
                }
                //Summry:
                //“you”: 1
                //“why”: 2
                //“tended mutual”: 1
            }
            System.Console.ReadKey();
        }
    }
}
